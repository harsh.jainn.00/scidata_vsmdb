%% Authors: Kilin Shi, Sven Schellenberger
% Copyright (C) 2019  Kilin Shi, Sven Schellenberger

%%%%%% IMPORTANT %%%%%%
% Copy the data from figshare to a subfolder "datasets" inside the folder of
% this script or change datadir below to the according folder where the
% datasets are stored

% Load the dataset you want to view and click "Run" in MATLAB to start this
% script
%%%%%%%%%%%%%%%%%%%%%%%

%% INIT
% addpath(genpath('Algorithms'))
addpath(genpath('Utils'))
datadir = 'datasets';

%% Configuration

SHOW_ELLIPSE_REKO = 1;

%% PREPROCESSING

% Fix jump at the beginning of some signals, caused by internal error of
% the ADC
[ecg_lead3, ecg_lead2, respiration, radar_I, radar_Q] = fixJumpAtStart(ecg_lead3, ecg_lead2, respiration, radar_I, radar_Q);

% Ellipsen Rekonstruktion
[~, ~, ~, dist] = ellipsenRekoMain(radar_I, radar_Q, 2, SHOW_ELLIPSE_REKO, Fs, measurement_info{1});

%% Get ECG signals

%offset korrektur
ecg_lead2_temp=ecg_lead3-mean(ecg_lead3);
ecg_lead3_temp=ecg_lead2-mean(ecg_lead2);
ecg_lead1_temp=ecg_lead2_temp-ecg_lead3_temp;

ecg_lead2_filtered=filtButter(ecg_lead2_temp, Fs, 4, [40/60 1000/60], 'bandpass');
ecg_lead3_filtered=filtButter(ecg_lead3_temp, Fs, 4, [40/60 1000/60], 'bandpass');
ecg_lead1_filtered=filtButter(ecg_lead1_temp, Fs, 4, [40/60 1000/60], 'bandpass');

% norm. between -1 and 1
ecg_lead1_norm=norm1to1(ecg_lead1_filtered);
ecg_lead2_norm=norm1to1(ecg_lead2_filtered);
ecg_lead3_norm=norm1to1(ecg_lead3_filtered);

% Get R and T-End locations
[abl,locsR,locsT] = getEcgLocs(datadir, measurement_info{1});

%% Get vital sign data

[signal_ton,pcg_audio_filt,signal_pulse,breathing] = getVitalSigns(dist,pcg_audio,respiration,Fs);

%% Get cutted signals

[begin_cut, end_cut] = loadCuts('ecgcuts_db.csv', measurement_info{1});
begin_cut = str2num(begin_cut);
end_cut = str2num(end_cut);

[~,locsR,locsT] = getEcgLocs(datadir, measurement_info{1});

% Cut signals
[signal_ton, pcg_audio_filt, locsR, locsT] = cutSignalsAndLocs(signal_ton, pcg_audio_filt, locsR, locsT, begin_cut, end_cut);

ecg_lead1_norm = ecg_lead1_norm(begin_cut:begin_cut+length(signal_ton)-1);
ecg_lead2_norm = ecg_lead2_norm(begin_cut:begin_cut+length(signal_ton)-1);
ecg_lead3_norm = ecg_lead3_norm(begin_cut:begin_cut+length(signal_ton)-1);
signal_pulse = signal_pulse(begin_cut:begin_cut+length(signal_ton)-1);
breathing = breathing(begin_cut:begin_cut+length(signal_ton)-1);
dist = dist(begin_cut:begin_cut+length(signal_ton)-1);

% Norm signals
signal_ton_norm = norm1to1(signal_ton); %norm. between -1 and 1
signal_pulse_norm = norm1to1(signal_pulse);
pcg_audio_filt_norm = norm1to1(pcg_audio_filt);
pcg_audio_filt_norm = pcg_audio_filt_norm - mean(pcg_audio_filt_norm);
% breathing_norm = norm1to1(breathing);
breathing_norm=normPercentile(breathing,95);
dist_norm = norm1to1(dist);

%% Get reference states of HSMM

% PCG Audio states and power
PCG_Features = getSpringerPCGFeaturesNoResampling(pcg_audio_filt_norm, Fs);
PCG_audio_states = labelPCGStates(PCG_Features(:,1),locsR, locsT, Fs);

% Radar states and power
PCG_Features = getSpringerPCGFeaturesNoResampling(signal_ton_norm, Fs);
PCG_radar_states = labelPCGStates(PCG_Features(:,1),locsR, locsT, Fs);

%% Get test states of HSMM

load('Springer_B_matrix.mat');
load('Springer_pi_vector.mat');
load('Springer_total_obs_distribution.mat');
radar_states = runSpringerSegmentationAlgorithm(signal_ton_norm, Fs, Springer_B_matrix, Springer_pi_vector, Springer_total_obs_distribution, 0);

%% Plot Heart Sounds and Pulse

time = 0:1/Fs:(length(signal_ton_norm)-1)/Fs;

scrsz = get(groot,'ScreenSize');
figure('Position',[1 1 scrsz(3) scrsz(4)-80]);
ax1 = subplot(4,1,1);
switch abl
    case 1
        plot(time,ecg_lead1_norm,'k-');
    case 2
        plot(time,ecg_lead2_norm,'k-');
    case 3
        plot(time,ecg_lead3_norm,'k-');
end
hold on;
str = sprintf(['(Dataset: %s)\nECG Lead %i'], measurement_info{1}, abl);
title(str, 'interpreter', 'none')
r_vline = vline(locsR(1)/Fs,'r-');
t_line = vline(locsT(1)/Fs,'b-');
set(r_vline,'HandleVisibility','on'); % Turn the legend on for vline
set(t_line,'HandleVisibility','on');
vline(locsR/Fs,'r-');
vline(locsT/Fs,'b-');
legend('ECG signal', 'ECG R-peaks', 'ECG T-wave ends')

ax2 = subplot(4,1,2);
plot(time,signal_ton.*1000000,'k-');
hold on;
plot(time,PCG_radar_states.*4);
ylabel('Distance (\mu m)');
hold on;
str = sprintf('Radar Heart Sounds');
title(str)
r_vline = vline(locsR(1)/Fs,'r-');
t_line = vline(locsT(1)/Fs,'b-');
set(r_vline,'HandleVisibility','on'); % Turn the legend on for vline
set(t_line,'HandleVisibility','on');
vline(locsR/Fs,'r-');
vline(locsT/Fs,'b-');
legend('Radar heart sound signal', 'Labeled states', 'ECG R-peaks', 'ECG T-wave ends')

ax3 = subplot(4,1,3);
plot(time,pcg_audio_filt_norm,'k-');
hold on;
str = sprintf('PCG Heart Sounds');
title(str)
r_vline = vline(locsR(1)/Fs,'r-');
t_line = vline(locsT(1)/Fs,'b-');
set(r_vline,'HandleVisibility','on'); % Turn the legend on for vline
set(t_line,'HandleVisibility','on');
vline(locsR/Fs,'r-');
vline(locsT/Fs,'b-');
legend('PCG heart sound signal', 'ECG R-peaks', 'ECG T-wave ends')

ax4 = subplot(4,1,4);
plot(time,signal_pulse.*1000,'k-');
hold on;
ylabel('Distance (mm)');
xlabel('Time (s)');
str = sprintf('Pulse signal radar (Heartbeat)');
title(str)
r_vline = vline(locsR(1)/Fs,'r-');
set(r_vline,'HandleVisibility','on'); % Turn the legend on for vline
vline(locsR/Fs,'r-');
legend('Radar pulse signal', 'ECG R-peaks')

linkaxes([ax1 ax2 ax3 ax4],'x');

%% Plot all Signals
figure('Position',[1 1 scrsz(3) scrsz(4)-80]);
ax(1) = subplot(4,1,1);
plot(time,dist,'k-');
title('Radar')
ylabel('Rel. Distance(m)');
% xlabel('Time(s)')

ax(2) = subplot(4,1,2);
plot(time,breathing,'k-')
title('Respiration Sensor')
ylabel('Voltage (mV)');
% xlabel('Time(s)')

ax(3) = subplot(4,1,3);
plot(time,ecg_lead1_norm+4,'k-');
hold on;
plot(time,ecg_lead2_norm+2,'b-');
plot(time,ecg_lead3_norm,'r-');
legend('Lead 1','Lead 2','Lead 3')
title('Electrocardiogram')
ylabel('norm. Amplitude');
% xlabel('Time(s)')

ax(4) = subplot(4,1,4);
plot(time,norm1to1(pcg_audio_filt),'k-')
title('Stethoscope (PCG)')
ylabel('norm. Amplitude');
xlabel('Time(s)')

linkaxes(ax,'x');
xlim([time(1) time(end)]);

%% Save data
% %%% Distance variation
% step=10;
% start_cut = 0*Fs+1;
% end_cut = 31*Fs;
% % signal_ton = norm1to1(signal_ton).*5;
% signal_ton_norm = normPercentile(signal_ton, 98);
% signal_ton_norm(signal_ton_norm > 1) = 1;
% signal_ton_norm(signal_ton_norm < -1) = -1;
% signal_pulse_norm = normPercentile(signal_pulse, 98);
% signal_pulse_norm(signal_pulse_norm > 1) = 1;
% signal_pulse_norm(signal_pulse_norm < -1) = -1;
% M1 = [time(start_cut:step:end_cut)', norm1to1(dist(start_cut:step:end_cut))+8, norm1to1(breathing(start_cut:step:end_cut))+6,...
%     ecg_lead1_norm(start_cut:step:end_cut)+4, ecg_lead2_norm(start_cut:step:end_cut)+2, ecg_lead3_norm(start_cut:step:end_cut),...
%     norm1to1(pcg_audio_filt(start_cut:step:end_cut)).*2-5, signal_ton_norm(start_cut:step:end_cut).*2-9,...
%     signal_pulse_norm(start_cut:step:end_cut)-2];
% fid = fopen('SAVED_DATA/signals_distvar.dat','wt');
% fprintf(fid,'t dist breathing ecg1 ecg2 ecg3 pcg hs pulse\n');
% dlmwrite('SAVED_DATA/signals_distvar.dat',M1,'-append','delimiter',' ','precision','%.3f');
% fclose(fid);

%%% Apnea
% step=10;
% start_cut = 15*Fs+1;
% end_cut = 46*Fs;
% M1 = [time(start_cut:step:end_cut)'-15, norm1to1(dist(start_cut:step:end_cut))+8, norm1to1(breathing(start_cut:step:end_cut))+6,...
%     ecg_lead1_norm(start_cut:step:end_cut)+4, ecg_lead2_norm(start_cut:step:end_cut)+2, ecg_lead3_norm(start_cut:step:end_cut),...
%     norm1to1(pcg_audio_filt(start_cut:step:end_cut)).*2-5, norm1to1(signal_ton(start_cut:step:end_cut)).*2-9,...
%     norm1to1(signal_pulse(start_cut:step:end_cut))-2];
% fid = fopen('SAVED_DATA/signals_apnea.dat','wt');
% fprintf(fid,'t dist breathing ecg1 ecg2 ecg3 pcg hs pulse\n');
% dlmwrite('SAVED_DATA/signals_apnea.dat',M1,'-append','delimiter',' ','precision','%.3f');
% fclose(fid);
