function [filteredIdx] = filterIdx(idxArray, filter_config)
%% Authors: Kilin Shi, Sven Schellenberger
% Copyright (C) 2017  Kilin Shi, Sven Schellenberger

%% DEFINES:

% Idx of all carotid measurements
carotid_p1_idx = [27:29];
carotid_p2_idx = [40:41];
carotid_p3_idx = [59,60];
carotid_p4_idx = [80:82];
carotid_p5_idx = [98:99, 101:102];
carotid_p6_idx = [103:104];
carotid_p7_idx = [136:140];
carotid_p8_idx = [164:166];
carotid_p9_idx = [190:192];
carotid_p10_idx = [214:215];
carotid_p11_idx = [225:226];
carotid_idx = [carotid_p1_idx,carotid_p2_idx,carotid_p3_idx,carotid_p4_idx,...
    carotid_p5_idx,carotid_p6_idx,carotid_p7_idx,carotid_p8_idx,carotid_p9_idx,carotid_p10_idx,carotid_p11_idx];

% Idx of all distance variation measurements
var_p2_idx = [239];
var_p3_idx = [61:62];
var_p5_idx = [93,100];
var_p6_idx = [108:109];
var_p7_idx = [142:143, 260:261];
var_p8_idx = [161];
var_p9_idx = [193];
var_p10_idx = [207:208];
var_p11_idx = [224];
var_idx = [var_p2_idx,var_p3_idx,var_p5_idx,var_p6_idx,var_p7_idx,var_p8_idx,var_p9_idx,var_p10_idx,var_p11_idx];

% Idx of all speech measurements
spe_p3_idx = [63];
spe_p5_idx = [92];
spe_p7_idx = [146];
spe_p9_idx = [178:179];
spe_p10_idx = [203];
spe_idx = [spe_p3_idx,spe_p5_idx,spe_p7_idx,spe_p9_idx,spe_p10_idx];

% Idx of all back measurements
bk_p1_idx = [31];
bk_p4_idx = [79];
bk_p5_idx = [96:97];
bk_p6_idx = [122];
bk_p7_idx = [123:124, 259];
bk_p9_idx = [189];
bk_p10_idx = [210:211];
bk_p11_idx = [229];
bk_idx = [bk_p1_idx, bk_p4_idx, bk_p5_idx, bk_p6_idx, bk_p7_idx, bk_p9_idx, bk_p10_idx, bk_p11_idx];

% Idx of all angle variation measurements
av_idx = [237,238,56,57,89,90,91,116,117,118,133,134,157,158,159,175,176,177,204,205,206,221,222,223];

%% CONFIG:
% filter_config:
% 1: normal measurements
% 2: carotid measurements
% 3: distance variation measurements
% 4: speech measurements
% 5: back measurements
% 6: angle variation measurements

if filter_config == 1
    FILTER_CAROTID = 1;
    FILTER_DISTANCE_VARIATION = 1;
    FILTER_SPEECH = 1;
    FILTER_BACK = 1;
    FILTER_AV = 1;
elseif filter_config == 2
%     filteredIdx = carotid_idx;
    filteredIdx = intersect(idxArray,carotid_idx);
    return;
elseif filter_config == 3
    filteredIdx = var_idx;
    filteredIdx = intersect(idxArray,var_idx);
    return;
elseif filter_config == 4
    filteredIdx = spe_idx;
    filteredIdx = intersect(idxArray,spe_idx);
    return;
elseif filter_config == 5
    filteredIdx = bk_idx;
    filteredIdx = intersect(idxArray,bk_idx);
    return;
elseif filter_config == 6
    filteredIdx = av_idx;
    filteredIdx = intersect(idxArray,av_idx);
    return;
end

%% FILTER INDICES

filteredIdx = idxArray;
if FILTER_CAROTID
   temp = intersect(filteredIdx,carotid_idx);
   for i = 1:length(temp)
       filteredIdx(filteredIdx == temp(i)) = [];
   end
end
if FILTER_DISTANCE_VARIATION
   temp = intersect(filteredIdx,var_idx);
   for i = 1:length(temp)
       filteredIdx(filteredIdx == temp(i)) = [];
   end
end
if FILTER_SPEECH
   temp = intersect(filteredIdx,spe_idx);
   for i = 1:length(temp)
       filteredIdx(filteredIdx == temp(i)) = [];
   end
end
if FILTER_BACK
   temp = intersect(filteredIdx,bk_idx);
   for i = 1:length(temp)
       filteredIdx(filteredIdx == temp(i)) = [];
   end
end
if FILTER_AV
   temp = intersect(filteredIdx,av_idx);
   for i = 1:length(temp)
       filteredIdx(filteredIdx == temp(i)) = [];
   end
end

end

