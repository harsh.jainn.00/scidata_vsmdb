function [channel1_raw, channel2_raw, channel5_raw, channel7_raw, channel8_raw] = fixJumpAtStart(channel1_raw, channel2_raw, channel5_raw, channel7_raw, channel8_raw)
% function [channel1_raw, channel2_raw, channel5_raw, channel7_raw, channel8_raw] = fixJumpAtStart(channel1_raw, channel2_raw, channel5_raw, channel7_raw, channel8_raw)
% remove jump at the beginnig of recording
% input: all channels
% output: all channels fixed
%% Authors: Kilin Shi, Sven Schellenberger
% Copyright (C) 2017  Kilin Shi, Sven Schellenberger

    med = median(channel1_raw(1:8));
    for i = 1:3
        if abs(channel1_raw(i)-med) > 0.05*med;
            channel1_raw(i) = med;
        end
    end
    med = median(channel2_raw(1:8));
    for i = 1:3
        if abs(channel2_raw(i)-med) > 0.05*med;
            channel2_raw(i) = med;
        end
    end
    med = median(channel5_raw(1:8));
    for i = 1:3
        if abs(channel5_raw(i)-med) > 0.05*med;
            channel5_raw(i) = med;
        end
    end
    med = median(channel7_raw(1:8));
    for i = 1:3
        if abs(channel7_raw(i)-med) > 0.05*med;
            channel7_raw(i) = med;
        end
    end
    med = median(channel8_raw(1:8));
    for i = 1:3
        if abs(channel8_raw(i)-med) > 0.05*med;
            channel8_raw(i) = med;
        end
    end

end