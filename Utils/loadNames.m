function [db_timestamps] = loadNames(dbfilewithpath)
% function [db_timestamps] = loadNames(syncwithpath)
% load all timestamps from a db-file (ecgcuts_db, sync_db)
% input: db-file
% output: timestamps
%% Authors: Kilin Shi, Sven Schellenberger
% Copyright (C) 2017  Kilin Shi, Sven Schellenberger

% syncfile with path
% bsp: 'C:\sync_db.csv'
if isempty(dbfilewithpath) || ~exist(dbfilewithpath, 'file')
   error('no sync file') 
end

%% LOAD SYNC_DB
fileID = fopen(dbfilewithpath,'r');
data = textscan(fileID,'%s', 'Delimiter', ';');
fclose(fileID);

if isempty(data)
   error('empty sync file'); 
end

db_timestamps = data{1}(2:end,1); % cut column name, timestamp identifies file