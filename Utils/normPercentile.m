function signal_out = normPercentile(signal, percentile)
% function signal_out = normPercentile(signal, percentile)
% normalise signal to the xxth percentile
% input: signal, percentile [0,100]
% output: normalised signal
%% Authors: Kilin Shi, Sven Schellenberger
% Copyright (C) 2017  Kilin Shi, Sven Schellenberger

signal = signal - median(signal);
sig_per = prctile(signal,percentile);
signal_out = signal./sig_per;
