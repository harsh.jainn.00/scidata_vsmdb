function [diff_rmse, ibi_ekg_rmse, ibi_test_rmse] = getIbiScore(locsR, locs_test, medfilt_size, smooth_size, Fs)
%% Authors: Kilin Shi, Sven Schellenberger
% Copyright (C) 2017  Kilin Shi, Sven Schellenberger

%% Returns the RMSE of the IBIs in ms
% locsR: location of locs of ECG in samples
% locs_test: location of test locs in samples

if length(locs_test) <= 1
    disp('empty locs');
end

locsR = locsR./Fs;
locs_test = locs_test./Fs;

% IBI ECG
ibi_ekg = diff(locsR);
ibi_ekg = medfilt1(ibi_ekg,medfilt_size,'truncate');
ibi_ekg = smooth(ibi_ekg,smooth_size);
ibi_ekg = interp1(locsR(2:end),ibi_ekg,locsR(2):1/Fs:locsR(end));

% IBI Test
ibi_test = diff(locs_test);
ibi_test = medfilt1(ibi_test,medfilt_size,'truncate');
ibi_test = smooth(ibi_test,smooth_size);
ibi_test = interp1(locs_test(2:end),ibi_test,locs_test(2):1/Fs:locs_test(end));

% Calculate RMSE
if locsR(2) > locs_test(2)
    ibi_test_rmse = ibi_test(round((locsR(2)-locs_test(2))*Fs):end);
    ibi_ekg_rmse = ibi_ekg;
elseif locsR(2) < locs_test(2)
    ibi_test_rmse = ibi_test;
    ibi_ekg_rmse = ibi_ekg(round((locs_test(2)-locsR(2))*Fs):end);
else
    ibi_ekg_rmse = ibi_ekg;
    ibi_test_rmse = ibi_test;
end
if length(ibi_test_rmse) > length(ibi_ekg_rmse)
    ibi_test_rmse = ibi_test_rmse(1:length(ibi_ekg_rmse));
elseif length(ibi_test_rmse) < length(ibi_ekg_rmse)
    ibi_ekg_rmse = ibi_ekg_rmse(1:length(ibi_test_rmse));
end
diff_rmse = sqrt(mean((ibi_ekg_rmse - ibi_test_rmse).^2))*1000; % RMSE in ms

end

