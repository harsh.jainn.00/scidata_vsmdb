%% Authors: Kilin Shi, Sven Schellenberger
% Copyright (C) 2019  Kilin Shi, Sven Schellenberger

%%%%%% IMPORTANT %%%%%%
% If your PC does not run 64Bit Windows, go to
% Utils/HSMM/default_Springer_HSMM_options.m and set
% springer_options.use_mex to "false"

% Copy the data from figshare to a subfolder "datasets" inside the folder of
% this script or change datadir below to the according folder where the
% datasets are stored
%%%%%%%%%%%%%%%%%%%%%%%

%% CONFIG
addpath(genpath('Utils'))
if ~exist('datasets', 'dir')
  mkdir('datasets');
end
datadir = 'datasets'; % Path to your datasets
db_size = 265;
numberOfPersons = 11;

%% INIT
db_names = loadNames('database.csv');

p1_idx = 1:31;                          % Person 1
p2_idx = [32:44, 230:247, 262:265];     % Person 2
p3_idx = 45:65;                         % Person 3
p4_idx = 66:82;                         % Person 4
p5_idx = 83:102;                        % Person 5
p6_idx = 103:122;                       % Person 6
p7_idx = [123:147, 248:261];            % Person 7
p8_idx = 148:166;                       % Person 8
p9_idx = 167:193;                       % Person 9
p10_idx = 194:215;                      % Person 10
p11_idx = 216:229;                      % Person 11
p_idx = {p1_idx, p2_idx, p3_idx, p4_idx, p5_idx, p6_idx, p7_idx, p8_idx, p9_idx, p10_idx, p11_idx};

%% LOAD ALL DATA FOR FASTER ACCESS DURING TRAINING AND TESTING

if ~exist('dataPCG', 'var')
    % Load or generate datasets for training
    if exist('database.mat', 'file')
        % Load database if it hase been created before
        disp('Loading database, please wait.')
        load('database.mat');
    else 
        dataPCG = {};
        dataRadar = {};
        labelsECG = {};
        FsAllData = [];

        disp('Creating database, please wait...')
        for db_i = 1:db_size

            run_config = 1:db_size;

            % Load data file
            datafile = findFile(datadir,['DATASET_', db_names{run_config(db_i)}]);
            if ~isempty(datafile)
                load(datafile);
            else
                error('datafile empty string');
            end

            % Get PCG and radar heartsound signals
            [~,~,signal_ton,~,locsR,locsT,~,pcg_audio_filt] = getFilteredCutSignals(ecg_lead3, ecg_lead2, respiration, radar_I, radar_Q, Fs, measurement_info, pcg_audio, datadir);
            
            dataPCG{1,db_i} = pcg_audio_filt;
            dataRadar{1,db_i} = signal_ton;
            labelsECG{db_i,1} = locsR;
            labelsECG{db_i,2} = locsT;
            FsAllData(1,db_i) = Fs;

            db_i
        end
        
        disp('Database creation finished, please wait for Matlab so save the database locally.')
        save('database.mat','dataPCG','dataRadar','labelsECG','FsAllData');
        disp('Database finished saving.')
    end
end

%% RUN TRAINING AND TEST ON ALL POSSIBLE SPLITS

ibi_radar = [];
ibi_ecg = [];
rmse_dataset = [];

dr = parallel.pool.Constant(dataRadar);
dp = parallel.pool.Constant(dataPCG);
fsAll = parallel.pool.Constant(FsAllData);
disp('Training and testing started, please wait...')

parfor split = 1:numberOfPersons
    
    test_id = split;
    test_idx = p_idx{test_id};
    test_idx = filterIdx(test_idx,1); % Only take measurements in default scenario
    train_id = setdiff(1:numberOfPersons,test_id);
    train_idx = cell2mat(p_idx(train_id));
    train_idx = filterIdx(train_idx,1); % Only take measurements in default scenario

    %% Train HSMM
    
    fprintf('Training started...\n');
    [B_matrix, pi_vector, total_obs_distribution] = trainSpringerSegmentationAlgorithm(dataRadar(train_idx), labelsECG(train_idx,:), FsAllData(train_idx), 0);
    fprintf('Training finished!\n');
    
    %% Test HSMM
    for i = 1:length(test_idx)
        
        datasetIdx = test_idx(i);
        locsR = labelsECG{datasetIdx,1};
        locsT = labelsECG{datasetIdx,2};
        
        fprintf('Testing dataset: %i\n',datasetIdx);
        
        signal_ton_states = runSpringerSegmentationAlgorithm(dr.Value{1,datasetIdx}, fsAll.Value(datasetIdx), B_matrix, pi_vector, total_obs_distribution, 0);
%         signal_ton_states = runSpringerSegmentationAlgorithm(dataRadar{datasetIdx}, FsAllData(datasetIdx), B_matrix, pi_vector, total_obs_distribution, 0);
        [diff_rmse, ~, ~] = getIbiScore(locsR, statesToLocs(signal_ton_states, 1), 5, 6, fsAll.Value(datasetIdx));
%         [diff_rmse, ~, ~] = getIbiScore(locsR, statesToLocs(signal_ton_states, 1), 5, 6, FsAllData(datasetIdx));
        rmse_dataset = [rmse_dataset, [diff_rmse; datasetIdx]];
        [ibi_ekg_temp, ibi_radar_temp] = getIbisNoInterp(locsR, statesToLocs(signal_ton_states, 1), 5, 6, fsAll.Value(datasetIdx));
%         [ibi_ekg_temp, ibi_radar_temp] = getIbisNoInterp(locsR, statesToLocs(signal_ton_states, 1), 5, 6, FsAllData(datasetIdx));
        ibi_radar = [ibi_radar, ibi_radar_temp];
        ibi_ecg = [ibi_ecg, ibi_ekg_temp];
        
    end
end
disp('Training and testing finished.\n')

%% Calculate scores
corr_radar_ecg = corr(ibi_ecg',ibi_radar');
diff_radar_ecg = abs(ibi_ecg - ibi_radar);
rmse_radar_ecg = sqrt(mean(diff_radar_ecg.^2))*1000;
percentage_smaller_10ms = sum(diff_radar_ecg < 0.010)/length(diff_radar_ecg);
percentage_smaller_20ms = sum(diff_radar_ecg < 0.020)/length(diff_radar_ecg);
percentile_90 = prctile(diff_radar_ecg,90);

%% Plot scatter and histogram

% Scatter plot
t = [0.4 2.2];
corr_axis = [0.4 2.2];

figure;
plot(ibi_ecg, ibi_radar,'r.','MarkerSize',1);
hold on
plot([0,3],[0,3],'k-')
title('Scatter plot')
axis equal; grid on;
ylim([0.4 1.4]);
xlim([0.4 1.4]);
xlabel('IBI ECG (s)');
ylabel('IBI Radar (s)');

% Histogram RMSE
rmse_plot = rmse_dataset(1,:);
figure;
histogram(rmse_plot,'BinWidth',10);
grid on;
title('Histogram RMSE');
xlabel('RMSE (ms)');
ylabel('Number of datasets');

% Histogram single IBI values
figure;
devs = abs(ibi_ecg - ibi_radar);
histogram(devs,'BinWidth',0.01);
grid on;
title('Histogram single IBI values')
xlabel('Deviation (s)');
ylabel('Number of IBI values');

%% Plot Bland Altman
[means,diffs,meanDiff,CR,~] = BlandAltman(ibi_radar,ibi_ecg,2,0);
[~,diffsPerct,meanDiffPerct,CRPerct,~] = BlandAltman(ibi_radar,ibi_ecg,2,1);
[meansBPM,diffsBPM,meanDiffBPM,CRBPM,~] = BlandAltman(60./ibi_radar,60./ibi_ecg,2,0);

vec = linspace(0.45,1.20,length(means));
vec2 = linspace(40,130,length(means));